from flask import Flask, render_template, redirect, request, jsonify

from pytube import YouTube

def get_quality_list(video):
    quality_list = []
    existing_qualities = set()

    for stream in video.streams.filter(file_extension='mp4'):
        quality = stream.resolution
        download_url = stream.url

        # Filter out duplicate or empty quality values
        if quality and quality not in existing_qualities:
            existing_qualities.add(quality)
            quality_list.append({'quality': quality, 'download_url': download_url})

    return quality_list

def get_video_info(url):
    try:
        video = YouTube(url)
        thumbnail = video.thumbnail_url
        title = video.title
        # quality_list = []
        # for stream in video.streams.filter(file_extension='mp4'):
        #     quality = stream.resolution
        #     download_url = stream.url

        #     quality_list.append({
        #         'quality': quality,
        #         'download_url': download_url
        #     })

        quality_list = get_quality_list(video)

        return thumbnail, quality_list, title
    except Exception as e:
        return None, None, None



# Create a Flask web application
app = Flask(__name__)

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/get-info', methods=['POST'])
def get_info():
    if request.method == 'POST':
        try:
            url = request.form['url']
            thumbnail, quality_list, title = get_video_info(url)
            if not thumbnail or not quality_list: 
                return jsonify({'error': 'Invalid Url'}), 400
            return jsonify({'thumbnail': thumbnail, 'quality_list': quality_list, 'title': title})
            
        except Exception as e:
        # Handle the exception and return a custom error response
            return jsonify({'error': 'Something went wrong'}), 400

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)